
const gulp = require('gulp');
const sass = require('gulp-sass');
jade = require('gulp-jade');
const imagemin = require('gulp-imagemin');
const rigger = require('gulp-rigger');
const runSequence = require('gulp4-run-sequence');
const concatCss = require('gulp-concat-css');
var livereload = require('gulp-watch');
var livereload = require('gulp-livereload'); 

gulp.task('build', function (callback) {
  runSequence(
    'rigger',
    'sass',
    'jade-html',
    callback
  );
});

gulp.task('sass', async function(){
  return gulp.src('src/**/*.scss')
    .pipe(sass()) // Конвертируем Sass в CSS с помощью gulp-sass
    .pipe(concatCss("assembled2.css"))
    .pipe(gulp.dest('public/styles'))
});

gulp.task('rigger', async function () {
  gulp.src('src/wiews/template/pages/index2.jade') //Выберем файлы по нужному пути
      .pipe(rigger()) //Прогоним через rigger
      .pipe(gulp.dest('src/wiews/pages/index2/')) //Выплюнем их в папку build
});

gulp.task('jade-html', async function(){
  gulp.src('src/wiews/pages/index2/index2.jade')
    .pipe(jade())// Конвертируем jade в html с помощью gulp-jade
    .pipe(gulp.dest('public/html/'))
});

exports.imagemin = async () => (
  gulp.src('src/**/*.png')
      .pipe(imagemin())
      .pipe(gulp.dest('public/imgs'))
);
gulp.task("watch", function(){
  livereload.listen();
     gulp.watch('src/**/*.scss', gulp.parallel('build'));
     gulp.watch('src/**/*.jade', gulp.parallel('build'));
     gulp.watch('src/**/*.png', gulp.parallel('build'));
});